package bot

import (
	"context"
	"fmt"
	"ilyabank/server/db/model"
	"strconv"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"ilyabank/server/protobuf"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/pkg/errors"
)

type Bot struct {
	conn   *tgbotapi.BotAPI
	client account.AccountServiceClient
}

func New(token string, client account.AccountServiceClient) (*Bot, error) {
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't create new bot api")
	}
	return &Bot{
		conn:   bot,
		client: client,
	}, nil
}

func (bot *Bot) StartCommand(update tgbotapi.Update) error {
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Hi, i'm your banking of ilya bank")
	if _, err := bot.conn.Send(msg); err != nil {
		return err
	}
	return nil
}

func (bot *Bot) MeCommand(update tgbotapi.Update) error {
	userID := update.Message.From.ID
	acc, err := bot.client.GetAccount(context.TODO(), &account.TgID{
		Name: strconv.Itoa(userID),
	})
	if err != nil {
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Could not found your account")
		if _, err := bot.conn.Send(msg); err != nil {
			return err
		}
		return err
	}
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, fmt.Sprintf("🙎‍♂ %s %s\n 📱 %s\n 📧 %s", acc.User.FirstName,
		acc.User.SecondName, acc.User.MobilePhone, acc.User.Email))
	if _, err := bot.conn.Send(msg); err != nil {
		return err
	}
	return nil
}

func (bot *Bot) RegisterCommand(updates tgbotapi.UpdatesChannel, update tgbotapi.Update) error {
	userID := update.Message.From.ID
	_, err := bot.client.GetAccount(context.TODO(), &account.TgID{
		Name: strconv.Itoa(userID),
	})
	if err == nil {
		if err := bot.send(update.Message.Chat.ID, "You have been already registered"); err != nil {
			return err
		}
		return nil
	}
	if e, ok := status.FromError(err); ok {
		if e.Code() != codes.NotFound {
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Smth is wrong:(")
			if _, err := bot.conn.Send(msg); err != nil {
				return err
			}
			return err
		}
	}
	acc, err := bot.account(updates, update)
	if err != nil {
		return err
	}
	_, err = bot.client.CreateAccount(context.TODO(), &account.Account{
		TgID: &account.TgID{
			Name: strconv.Itoa(update.Message.From.ID),
		},
		User: &account.User{
			FirstName:   acc.User.FirstName,
			SecondName:  acc.User.SecondName,
			Email:       acc.User.Email,
			MobilePhone: acc.User.MobilePhone,
		},
	})
	if err != nil {
		if err := bot.send(update.Message.Chat.ID, "Could not register you, sorry"); err != nil {
			return err
		}
		return err
	}
	if err := bot.send(update.Message.Chat.ID, "You have been successfully registered"); err != nil {
		return err
	}
	return nil
}

func (bot *Bot) TransferCommand(updates tgbotapi.UpdatesChannel, update tgbotapi.Update) error {
	tgID, err := bot.msgFromChan(update.Message.Chat.ID, updates, "Write tg id")
	if err != nil {
		return err
	}
	amount, err := bot.msgFromChan(update.Message.Chat.ID, updates, "Write amount")
	if err != nil {
		return err
	}
	parsedAmount, err := strconv.ParseInt(amount, 10, 32)
	if err != nil {
		return err
	}
	_, err = bot.client.Transfer(context.Background(), &account.TransferRequest{
		From: &account.TgID{
			Name: strconv.FormatInt(int64(update.Message.From.ID), 10),
		},
		To: &account.TgID{
			Name: tgID,
		},
		Amount: float32(parsedAmount),
	})
	if err != nil {
		return err
	}
	return nil
}

func (bot *Bot) UpdatesChan(config tgbotapi.UpdateConfig) (tgbotapi.UpdatesChannel, error) {
	return bot.conn.GetUpdatesChan(config)
}

func (bot *Bot) account(updates tgbotapi.UpdatesChannel, update tgbotapi.Update) (model.Account, error) {
	var (
		account model.Account
		err     error
	)
	account.User.FirstName, err = bot.msgFromChan(update.Message.Chat.ID, updates, "What's you name")
	if err != nil {
		return model.Account{}, err
	}

	account.User.SecondName, err = bot.msgFromChan(update.Message.Chat.ID, updates, "What's you second name")
	if err != nil {
		return model.Account{}, err
	}

	account.User.MobilePhone, err = bot.msgFromChan(update.Message.Chat.ID, updates, "What's you mobile phone")
	if err != nil {
		return model.Account{}, err
	}

	account.User.Email, err = bot.msgFromChan(update.Message.Chat.ID, updates, "What's you mobile email?")
	if err != nil {
		return model.Account{}, err
	}
	return account, nil
}

func (bot *Bot) msgFromChan(chatID int64, updates tgbotapi.UpdatesChannel, message string) (string, error) {
	if err := bot.send(chatID, message); err != nil {
		return "", err
	}
	nameMsg := <-updates
	return nameMsg.Message.Text, nil
}

func (bot *Bot) send(chatID int64, message string) error {
	msg := tgbotapi.NewMessage(chatID, message)
	if _, err := bot.conn.Send(msg); err != nil {
		return err
	}
	return nil
}
