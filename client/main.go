package main

import (
	"log"
	"os"

	"google.golang.org/grpc"

	"ilyabank/client/bot"
	"ilyabank/server/protobuf"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

const botToken = "BOT_TOKEN"

func main() {
	token := os.Getenv(botToken)
	if token == "" {
		log.Fatalf(`No %q env var`, botToken)
	}
	conn, err := grpc.Dial(":5555", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("coudl not connect to server: %s", err)
	}
	defer conn.Close()
	client := account.NewAccountServiceClient(conn)
	tgBot, err := bot.New(token, client)
	if err != nil {
		log.Fatalf("could not create new bot api: %s", err)
	}
	u := tgbotapi.UpdateConfig{
		Timeout: 60,
	}
	updates, err := tgBot.UpdatesChan(u)

	for update := range updates {
		if update.Message == nil {
			continue
		}
		switch update.Message.Command() {
		case "start":
			if err := tgBot.StartCommand(update); err != nil {
				log.Print(err)
			}
		case "me":
			if err := tgBot.MeCommand(update); err != nil {
				log.Print(err)
			}
		case "register":
			if err := tgBot.RegisterCommand(updates, update); err != nil {
				log.Print(err)
			}
		case "transfer":
			if err := tgBot.TransferCommand(updates, update); err != nil {
			}
		}
	}
}
