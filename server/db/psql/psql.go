package psql

import (
	"context"
	"database/sql"

	"ilyabank/server/db/model"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

type Store struct {
	db       *sqlx.DB
	executor sqlx.ExtContext
}

func New(uri string) (*Store, error) {
	db, err := sqlx.Connect("postgres", uri)
	return &Store{db: db, executor: db}, errors.Wrap(err, "could not connect to db")
}

func (s *Store) RunInTransaction(ctx context.Context, f func(store model.AccountRepository) error) error {
	if _, ok := s.executor.(*sqlx.Tx); ok {
		return errors.New("nested transaction are not allowed")
	}
	tx, err := s.db.BeginTxx(ctx, &sql.TxOptions{
		Isolation: 2,
	})
	if err != nil {
		return errors.Wrap(err, "could not begin transaction")
	}
	txStore := &Store{
		db:       s.db,
		executor: tx,
	}
	err = f(txStore)
	if err != nil {
		if err = f(txStore); err != nil {
			if err := tx.Rollback(); err != nil {
				return errors.Wrap(err, "could not rollback transaction")
			}
			return err
		}
	}
	err = tx.Commit()
	return errors.Wrap(err, "could not commit transaction")
}
