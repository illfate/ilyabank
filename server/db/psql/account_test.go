package psql

import (
	"context"
	"github.com/stretchr/testify/require"
	"os"
	"testing"

	"ilyabank/server/db/model"

	_ "github.com/lib/pq"
	"github.com/stretchr/testify/assert"
)

const psqlURI = "TEST_PSQL_URI"

func TestAccount(t *testing.T) {
	tt := []struct {
		name      string
		account   model.Account
		firstErr  string
		secondErr string
	}{
		{
			name: "correct test",
			account: model.Account{
				TgID: "111",
				User: model.User{
					FirstName:   "q",
					SecondName:  "t",
					Email:       "test@mail",
					MobilePhone: "1234",
					Invoice: model.Invoice{
						Currency: model.BynCurrency,
						Amount:   200,
					},
				},
			},
		},
	}
	db, err := New(os.Getenv(psqlURI))
	if err != nil {
		t.Fatal(err)
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			tgID, err := db.CreateAccount(context.Background(), tc.account)
			if err != nil {
				assert.EqualError(t, err, tc.firstErr)
			}
			assert.Equal(t, tc.account.TgID, tgID)
			acc, err := db.Account(context.Background(), tgID)
			if err != nil {
				assert.EqualError(t, err, tc.firstErr)
			}
			assert.Equal(t, tc.account, acc)
		})
	}
}

func TestTransfer(t *testing.T) {
	tt := []struct {
		name      string
		firstAcc  model.Account
		secondAcc model.Account
		amount    float64
	}{
		{
			name: "correct test",
			firstAcc: model.Account{
				TgID: "333",
				User: model.User{
					FirstName:   "q",
					SecondName:  "t",
					Email:       "tmail",
					MobilePhone: "3334",
					Invoice: model.Invoice{
						Currency: model.BynCurrency,
						Amount:   200,
					},
				},
			},
			secondAcc: model.Account{
				TgID: "112",
				User: model.User{
					FirstName:   "q",
					SecondName:  "t",
					Email:       "test2@mail",
					MobilePhone: "12345",
					Invoice: model.Invoice{
						Currency: model.BynCurrency,
						Amount:   200,
					},
				},
			},
			amount: 77,
		},
	}
	db, err := New(os.Getenv(psqlURI))
	if err != nil {
		t.Fatal(err)
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			fromTgID, err := db.CreateAccount(context.Background(), tc.firstAcc)
			require.NoError(t, err)
			toTgID, err := db.CreateAccount(context.Background(), tc.secondAcc)
			require.NoError(t, err)
			err = db.Transfer(context.Background(), fromTgID, toTgID, tc.amount)
			assert.NoError(t, err)

			acc, err := db.Account(context.Background(), fromTgID)
			require.NoError(t, err)
			assert.Equal(t, tc.firstAcc.User.Invoice.Amount-tc.amount, acc.User.Invoice.Amount)

			acc, err = db.Account(context.Background(), toTgID)
			require.NoError(t, err)
			assert.Equal(t, tc.firstAcc.User.Invoice.Amount+tc.amount, acc.User.Invoice.Amount)
		})
	}
}
