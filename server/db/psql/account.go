package psql

import (
	"context"
	"database/sql"

	"ilyabank/server/db/model"

	"github.com/pkg/errors"
)

func (s *Store) CreateAccount(ctx context.Context, acc model.Account) (string, error) {
	_, err := s.executor.ExecContext(ctx,
		`INSERT INTO account(tg_id, first_name, second_name, phone, email, currency, amount)
					VALUES($1,$2,$3,$4,$5,$6,$7)`, acc.TgID, acc.User.FirstName, acc.User.SecondName,
		acc.User.MobilePhone, acc.User.Email, model.BynCurrency, model.DefaultAmount)
	if err != nil {
		return "", errors.Wrap(err, "could not insert new user")
	}
	return acc.TgID, nil
}

func (s *Store) Account(ctx context.Context, tgID string) (model.Account, error) {
	var acc model.Account
	err := s.executor.QueryRowxContext(ctx,
		`SELECT tg_id, first_name, second_name, email, phone, currency, amount
		   FROM account
		  WHERE tg_id = $1`, tgID).Scan(&acc.TgID, &acc.User.FirstName, &acc.User.SecondName, &acc.User.Email,
		&acc.User.MobilePhone, &acc.User.Invoice.Currency, &acc.User.Invoice.Amount)
	if err == sql.ErrNoRows {
		return model.Account{}, model.ErrNotFound
	}
	return acc, errors.Wrap(err, "could not get account")
}

func (s *Store) Transfer(ctx context.Context, fromTgID, toTgID string, amount float64) error {
	err := s.RunInTransaction(ctx, func(store model.AccountRepository) error {
		err := s.updateMoney(ctx, fromTgID, amount)
		if err != nil {
			return errors.Wrap(err, "could not update account money")
		}
		err = s.updateMoney(ctx, toTgID, -amount)
		return errors.Wrap(err, "could not update account money")
	})
	return err
}

func (s *Store) updateMoney(ctx context.Context, tgID string, amount float64) error {
	_, err := s.executor.ExecContext(ctx, `UPDATE account SET amount = amount - $1 WHERE tg_id = $2`, amount, tgID)
	return errors.Wrap(err, "could not update account money")
}
