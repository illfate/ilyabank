package mongo

//type DB struct {
//	mongo *mongo.Client
//	col   *mongo.Collection
//}
//
//func New(uri string) (*DB, error) {
//	client, err := mongo.NewClient(options.Client().ApplyURI(uri))
//	if err != nil {
//		return nil, errors.Wrap(err, "error occurred while establishing connection to mongoDB")
//	}
//
//	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
//	defer cancel()
//
//	err = client.Connect(ctx)
//	if err != nil {
//		return nil, err
//	}
//
//	err = client.Ping(context.Background(), nil)
//	if err != nil {
//		return nil, err
//	}
//	return &DB{mongo: client, col: client.Database("test").Collection("users")}, nil
//}
//
//func (db *DB) CreateAccount(ctx context.Context, acc model.Account) (string, error) {
//	res, err := db.col.InsertOne(ctx, acc)
//	if err != nil {
//		return "", err
//	}
//	return res.InsertedID.(string), nil
//}
//
//func (db *DB) Account(ctx context.Context, tgID string) (model.Account, error) {
//	var acc db.Account
//	err := db.col.FindOne(ctx, bson.M{
//		"_id": tgID,
//	}).Decode(&acc)
//	if err == mongo.ErrNoDocuments {
//		return db.Account{}, db.ErrNotFound
//	}
//	if err != nil {
//		return db.Account{}, err
//	}
//	return acc, nil
//}
//
//func (db *DB) Transfer(ctx context.Context, fromTgID, toPhone string, amount float64) error {
//	err := db.mongo.UseSession(ctx, func(sCtx mongo.SessionContext) error {
//		err := sCtx.StartTransaction()
//		if err != nil {
//			return err
//		}
//		defer sCtx.AbortTransaction(sCtx)
//		acc, err := db.Account(sCtx, fromTgID)
//		if err != nil {
//			return err
//		}
//		if acc.User.Invoice.Amount < amount {
//			return errors.New("not enough money")
//		}
//		err = db.updateAmount(sCtx, acc.User.MobilePhone, -amount)
//		if err != nil {
//			return err
//		}
//		err = db.updateAmount(sCtx, toPhone, amount)
//		if err != nil {
//			return err
//		}
//		if err := sCtx.CommitTransaction(sCtx); err != nil {
//			return err
//		}
//		return nil
//	})
//	return err
//}
//
//func (db *DB) updateAmount(ctx context.Context, phone string, amount float64) error {
//	f := bson.M{
//		"mobilephone": phone,
//	}
//	update := bson.M{
//		"$inc": bson.M{
//			"invoice.amount": amount,
//		},
//	}
//	_, err := db.col.UpdateOne(ctx, f, update)
//	if err != nil {
//		return err
//	}
//	return nil
//}
