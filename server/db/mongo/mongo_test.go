package mongo

//import (
//	"context"
//	"os"
//	"testing"
//	"time"
//
//	"ilyabank/server/model"
//
//	"github.com/stretchr/testify/assert"
//	"github.com/stretchr/testify/require"
//)
//
//const mongoURI = "TEST_MONGODB_URI"
//
//func TestUser(t *testing.T) {
//	tt := []struct {
//		name    string
//		account model.Account
//	}{
//		{
//			name: "first_test",
//			account: model.Account{
//				TgID: "345347433",
//				User: model.User{
//					FirstName:   "alal",
//					SecondName:  "apa",
//					Email:       "test@mail.com",
//					MobilePhone: "+375290000000",
//					Cards: []model.Card{
//						{
//							Number:          "0000 0000 0000 0000",
//							PersonFullName:  "alal apa",
//							ProviderName:    model.Mastercard,
//							CVV:             000,
//							ExpirationMonth: time.June,
//							ExpirationYear:  20,
//							Invoice: model.Invoice{
//								Currency: "BYN",
//								Amount:   20,
//							},
//						},
//					},
//				},
//			},
//		},
//	}
//	uri := os.Getenv(mongoURI)
//	if uri == "" {
//		t.Fatal("empty mongo uri")
//	}
//	db, err := New(uri)
//	require.NoError(t, err)
//	for _, tc := range tt {
//		t.Run(tc.name, func(t *testing.T) {
//			tgID, err := db.CreateAccount(context.Background(), tc.account)
//			require.NoError(t, err)
//			assert.Equal(t, tc.account.TgID, tgID)
//			acc, err := db.Account(context.Background(), tgID)
//			require.NoError(t, err)
//			assert.Equal(t, tc.account, acc)
//		})
//	}
//}
//
//func TestTransfer(t *testing.T) {
//	tt := []struct {
//		name         string
//		firstAcc     model.Account
//		secondAcc    model.Account
//		updateAmount float64
//	}{
//		{
//			name: "first_test",
//			firstAcc: model.Account{
//				TgID: "34321327433",
//				User: model.User{
//					FirstName:   "max",
//					SecondName:  "mxa",
//					Email:       "max@mail.com",
//					MobilePhone: "+375291111111",
//					Invoice: model.Invoice{
//						Currency: "BYN",
//						Amount:   150,
//					},
//				},
//			},
//			secondAcc: model.Account{
//				TgID: "34321327422",
//				User: model.User{
//					FirstName:   "ilya",
//					SecondName:  "hont",
//					Email:       "ilya@mail.com",
//					MobilePhone: "+375291111111",
//					Invoice: model.Invoice{
//						Currency: "BYN",
//						Amount:   150,
//					},
//				},
//			},
//			updateAmount: 150,
//		},
//	}
//	uri := os.Getenv(mongoURI)
//	if uri == "" {
//		t.Fatal("empty mongo uri")
//	}
//	db, err := New(uri)
//	require.NoError(t, err)
//	for _, tc := range tt {
//		t.Run(tc.name, func(t *testing.T) {
//			_, err := db.CreateAccount(context.Background(), tc.firstAcc)
//			require.NoError(t, err)
//			_, err = db.CreateAccount(context.Background(), tc.secondAcc)
//			require.NoError(t, err)
//			err = db.Transfer(context.Background(), tc.firstAcc.TgID, tc.secondAcc.User.MobilePhone, tc.firstAcc.User.Invoice.Amount)
//			require.NoError(t, err)
//			acc, err := db.Account(context.Background(), tc.firstAcc.TgID)
//			require.NoError(t, err)
//			assert.Equal(t, acc.User.Invoice.Amount-tc.updateAmount, acc.User.Invoice.Amount)
//			acc, err = db.Account(context.Background(), tc.secondAcc.TgID)
//			require.NoError(t, err)
//			assert.Equal(t, acc.User.Invoice.Amount-tc.updateAmount, acc.User.Invoice.Amount)
//		})
//	}
//}
