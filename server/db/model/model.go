package model

import (
	"context"
	"time"

	"github.com/pkg/errors"
)

type Provider string

const (
	Mastercard Provider = "Mastercard"
	Visa       Provider = "Visa"
)

const (
	BynCurrency   = "BYN"
	DefaultAmount = 200
)

var ErrNotFound = errors.New("not found")

type Account struct {
	TgID string `bson:"_id"`
	User User   `bson:"user"`
}

type User struct {
	FirstName   string
	SecondName  string
	Email       string
	MobilePhone string
	Invoice     Invoice
}

type Card struct {
	Number          string
	PersonFullName  string
	ProviderName    Provider
	CVV             uint8
	ExpirationMonth time.Month
	ExpirationYear  uint8
	Invoice         Invoice
}

type NewCardInput struct {
	Provider   Provider
	AliveYears uint8
}

type Invoice struct {
	Currency string
	Amount   float64
}

type AccountRepository interface {
	CreateAccount(ctx context.Context, acc Account) (string, error)
	Account(ctx context.Context, tgID string) (Account, error)
	Transfer(ctx context.Context, fromTgID, toTgID string, amount float64) error

	RunInTransaction(ctx context.Context, f func(store AccountRepository) error) error
}
