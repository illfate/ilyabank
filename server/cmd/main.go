package main

import (
	"ilyabank/server/db/psql"
	"log"
	"net"

	"ilyabank/server/protobuf"
	"ilyabank/server/rpc"

	"google.golang.org/grpc"

	_ "github.com/lib/pq"
)

func main() {
	list, err := net.Listen("tcp", ":5555")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	grpcServer := grpc.NewServer()
	db, err := psql.New("postgresql://ilya:bank@localhost:5432/bank?sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	rpc := rpc.New(db)
	account.RegisterAccountServiceServer(grpcServer, rpc)
	log.Print(grpcServer.Serve(list))
}
