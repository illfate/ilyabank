package rpc

import (
	"context"
	_ "github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"ilyabank/server/db/model"
	"ilyabank/server/db/psql"
	"os"
	"testing"
)

const psqlURI = "TEST_PSQL_URI"

//func TestCreateAccount(t *testing.T) {
//	tt := []struct {
//		name string
//		acc model.Account
//	}{
//		{
//			name: "",
//		},
//	}
//	db := new(mockdb.MockDB)
//	s:=New(db)
//	for _, tc := range tt {
//		t.Run(tc.name, func(t *testing.T) {
//			db.On("CreateAccount",)
//			s.CreateAccount(context.Background(),)
//
//		})
//	}
//}
//
//func TestGetAccount(t *testing.T) {
//
//}
//
//func TestTransfer(t *testing.T) {
//
//}

func TestRpc(t *testing.T) {
	tt := []struct {
		name string
		tgID string
		acc  model.Account
	}{
		{
			name: "correct test",
			tgID: "test",
			acc: model.Account{
				TgID: "test",
				User: model.User{
					FirstName:   "vlad",
					SecondName:  "byrik",
					Email:       "burik@mail.com",
					MobilePhone: "85834534",
				},
			},
		},
	}
	uri := os.Getenv(psqlURI)
	if uri == "" {
		t.Fatal("empty db uri")
	}
	db, err := psql.New(uri)
	if err != nil {
		t.Fatal(err)
	}
	s := New(db)
	require.NoError(t, err)
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			_, err := s.CreateAccount(context.TODO(), toBankAccount(tc.acc))
			require.NoError(t, err)
			acc, err := s.GetAccount(context.TODO(), toBankTgID(tc.tgID))
			require.NoError(t, err)
			assert.Equal(t, tc.acc, toAccount(acc))
		})
	}
}
