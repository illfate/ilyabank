package rpc

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"ilyabank/server/db/model"
	account "ilyabank/server/protobuf"
	"log"
)

type Server struct {
	account model.AccountRepository
}

func New(user model.AccountRepository) *Server {
	s := &Server{
		account: user,
	}
	return s
}

func (s *Server) CreateAccount(ctx context.Context, account *account.Account) (*empty.Empty, error) {
	acc := toAccount(account)
	_, err := s.account.CreateAccount(ctx, acc)
	if err != nil {
		log.Print(err)
		return nil, err
	}
	return &empty.Empty{}, nil
}
func (s *Server) GetAccount(ctx context.Context, tgID *account.TgID) (*account.Account, error) {
	acc, err := s.account.Account(ctx, tgID.GetName())
	if err == model.ErrNotFound {
		return nil, status.Error(codes.NotFound, "Account not found")
	}
	if err != nil {
		log.Print(err)
		return nil, err
	}
	return toBankAccount(acc), nil
}

func (s *Server) Transfer(ctx context.Context, req *account.TransferRequest) (*empty.Empty, error) {
	err := s.account.Transfer(ctx, req.From.GetName(), req.To.GetName(), float64(req.Amount))
	if err != nil {
		log.Print(err)

		return nil, err
	}
	return &empty.Empty{}, nil
}

func toBankAccount(acc model.Account) *account.Account {
	return &account.Account{
		TgID: toBankTgID(acc.TgID),
		User: toBankUser(acc.User),
	}
}

func toAccount(acc *account.Account) model.Account {
	if acc == nil {
		return model.Account{}
	}
	return model.Account{
		TgID: acc.TgID.GetName(),
		User: model.User{
			FirstName:   acc.User.GetFirstName(),
			SecondName:  acc.User.GetSecondName(),
			Email:       acc.User.GetEmail(),
			MobilePhone: acc.User.GetMobilePhone(),
		},
	}
}

func toBankTgID(tgID string) *account.TgID {
	return &account.TgID{
		Name: tgID,
	}
}

func toBankUser(user model.User) *account.User {
	return &account.User{
		FirstName:   user.FirstName,
		SecondName:  user.SecondName,
		Email:       user.Email,
		MobilePhone: user.MobilePhone,
	}
}
