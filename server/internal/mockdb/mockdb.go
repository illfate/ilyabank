package mockdb

import (
	"context"
	"ilyabank/server/db/model"

	"github.com/stretchr/testify/mock"
)

type MockDB struct {
	mock.Mock
}

func (mock *MockDB) RunInTransaction(ctx context.Context, f func(store model.AccountRepository) error) error {
	args := mock.Called(f)
	return args.Get(0).(error)
}

func (mock *MockDB) CreateAccount(ctx context.Context, acc model.Account) (string, error) {
	args := mock.Called(acc)
	return args.Get(0).(string), args.Get(1).(error)
}
func (mock *MockDB) Account(ctx context.Context, tgID string) (model.Account, error) {
	args := mock.Called(tgID)
	return args.Get(0).(model.Account), args.Get(1).(error)
}
func (mock *MockDB) Transfer(ctx context.Context, fromTgID, toTgID string, amount float64) error {
	args := mock.Called(fromTgID, toTgID, amount)
	return args.Get(1).(error)
}
