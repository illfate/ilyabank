bin/bank:
	go build -ldflags "-linkmode external -extldflags -static" -o bin/bank ./server/cmd

.Phony: gen
gen:
	protoc server/protobuf/account.proto --go_out=plugins=grpc:.

.PHONY: test-db
test-db:
	sudo docker-compose -f docker-compose.test.yml up -d
	sleep 1.5
	sql-migrate up
	go test -v -cover ./...

.PHONY: clean
clean:
	rm -rf bin

.PHONY: clean-test-data
clean-test-data:
	sudo docker-compose -f docker-compose.test.yml down
